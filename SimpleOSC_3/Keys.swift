//
//  Keys.swift
//  SimpleOSC_3
//
//  Created by Gian on 18/09/2019.
//  Copyright © 2019 Giannino Clemente. Licensed under GPLv3. See LICENSE for more details.
//

import UIKit
import SwiftOSC

class Keys: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var idStepper: UIStepper!
    
    @IBOutlet weak var VolSlider: UISlider!
    
    @IBOutlet weak var PanicBtn: UIButton!
    @IBOutlet weak var OctDownBtn: UIButton!
    @IBOutlet weak var OctUpBtn: UIButton!
    
    
    
    //Botonets
    @IBOutlet weak var Key0: UIButton!
    @IBOutlet weak var Key1: UIButton!
    @IBOutlet weak var Key2: UIButton!
    @IBOutlet weak var Key3: UIButton!
    @IBOutlet weak var Key4: UIButton!
    @IBOutlet weak var Key5: UIButton!
    @IBOutlet weak var Key6: UIButton!
    @IBOutlet weak var Key7: UIButton!
    @IBOutlet weak var Key8: UIButton!
    @IBOutlet weak var Key9: UIButton!
    @IBOutlet weak var Key10: UIButton!
    @IBOutlet weak var Key11: UIButton!
    @IBOutlet weak var Key12: UIButton!
    @IBOutlet weak var Key13: UIButton!
    @IBOutlet weak var Key14: UIButton!
    @IBOutlet weak var Key15: UIButton!
    @IBOutlet weak var Key16: UIButton!
    @IBOutlet weak var Key17: UIButton!
    @IBOutlet weak var Key18: UIButton!
    @IBOutlet weak var Key19: UIButton!
    @IBOutlet weak var Key20: UIButton!
    @IBOutlet weak var Key21: UIButton!
    @IBOutlet weak var Key22: UIButton!
    @IBOutlet weak var Key23: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Force Light Mode
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        idLabel.text = "ID: \(DeviceID)"
        idStepper.value = Double(DeviceID)
        // OUTport = 8000 + DeviceID
        // client = OSCClient(address: "255.255.255.255", port: OUTport)
        VolSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
    }
    
    //Change ID Stepper
    @IBAction func idStepperChanged(_ sender: Any) {
        DeviceID = Int(idStepper.value)
        idLabel.text = "ID: \(DeviceID)"
        OUTport = 8000 + DeviceID
        client = OSCClient(address: client.address, port: OUTport)
        
    }
    
    //VolChange
    @IBAction func VolSliderChange(_ sender: Any) {
        let OSCAdress = "/keys/vol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), VolSlider.value)
        client.send(message)
    }
    
    //PANIC
    @IBAction func PanicPressed(_ sender: Any) {
        let OSCAdress = "/midipanic"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    
    //Oct-
    @IBAction func OctDownPressed(_ sender: Any) {
        let OSCAdress = "/keys/oct-"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    
    //Oct+
    @IBAction func OctUpPressed(_ sender: Any) {
        let OSCAdress = "/keys/oct+"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    
    //KeyDowns
    
    @IBAction func Key0Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0, 1)
        client.send(message)
    }
    @IBAction func Key1Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1, 1)
        client.send(message)
    }
    @IBAction func Key2Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 2, 1)
        client.send(message)
    }
    @IBAction func Key3Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 3, 1)
        client.send(message)
    }
    @IBAction func Key4Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 4, 1)
        client.send(message)
    }
    @IBAction func Key5Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 5, 1)
        client.send(message)
    }
    @IBAction func Key6Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 6, 1)
        client.send(message)
    }
    @IBAction func Key7Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 7, 1)
        client.send(message)
    }
    @IBAction func Key8Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 8, 1)
        client.send(message)
    }
    @IBAction func Key9Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 9, 1)
        client.send(message)
    }
    @IBAction func Key10Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 10, 1)
        client.send(message)
    }
    @IBAction func Key11Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 11, 1)
        client.send(message)
    }
    @IBAction func Key12Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 12, 1)
        client.send(message)
    }
    @IBAction func Key13Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 13, 1)
        client.send(message)
    }
    @IBAction func Key14Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 14, 1)
        client.send(message)
    }
    @IBAction func Key15Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 15, 1)
        client.send(message)
    }
    @IBAction func Key16Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 16, 1)
        client.send(message)
    }
    @IBAction func Key17Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 17, 1)
        client.send(message)
    }
    @IBAction func Key18Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 18, 1)
        client.send(message)
    }
    @IBAction func Key19Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 19, 1)
        client.send(message)
    }
    @IBAction func Key20Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 20, 1)
        client.send(message)
    }
    @IBAction func Key21Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 21, 1)
        client.send(message)
    }
    @IBAction func Key22Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 22, 1)
        client.send(message)
    }
    @IBAction func Key23Down(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 23, 1)
        client.send(message)
    }
    
    //KeyUps
    
    @IBAction func Key0Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0, 0)
        client.send(message)
    }
    @IBAction func Key1Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1, 0)
        client.send(message)
    }
    @IBAction func Key2Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 2, 0)
        client.send(message)
    }
    @IBAction func Key3Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 3, 0)
        client.send(message)
    }
    @IBAction func Key4Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 4, 0)
        client.send(message)
    }
    @IBAction func Key5Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 5, 0)
        client.send(message)
    }
    @IBAction func Key6Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 6, 0)
        client.send(message)
    }
    @IBAction func Key7Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 7, 0)
        client.send(message)
    }
    @IBAction func Key8Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 8, 0)
        client.send(message)
    }
    @IBAction func Key9Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 9, 0)
        client.send(message)
    }
    @IBAction func Key10Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 10, 0)
        client.send(message)
    }
    @IBAction func Key11Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 11, 0)
        client.send(message)
    }
    @IBAction func Key12Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 12, 0)
        client.send(message)
    }
    @IBAction func Key13Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 13, 0)
        client.send(message)
    }
    @IBAction func Key14Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 14, 0)
        client.send(message)
    }
    @IBAction func Key15Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 15, 0)
        client.send(message)
    }
    @IBAction func Key16Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 16, 0)
        client.send(message)
    }
    @IBAction func Key17Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 17, 0)
        client.send(message)
    }
    @IBAction func Key18Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 18, 0)
        client.send(message)
    }
    @IBAction func Key19Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 19, 0)
        client.send(message)
    }
    @IBAction func Key20Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 20, 0)
        client.send(message)
    }
    @IBAction func Key21Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 21, 0)
        client.send(message)
    }
    @IBAction func Key22Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 22, 0)
        client.send(message)
    }
    @IBAction func Key23Up(_ sender: Any) {
        let OSCAdress = "/keys"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 23, 0)
        client.send(message)
    }

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
