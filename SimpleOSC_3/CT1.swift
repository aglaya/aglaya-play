//
//  CT1.swift
//  Aglaya PLAY
//
//  Created by Gian on 04/10/2019.
//  Copyright © 2019 Giannino Clemente. Licensed under GPLv3. See LICENSE for more details.
//

import UIKit
import SwiftOSC

var DlyOnOffState = false
var H1ButState = false
var H2ButState = false
var H3ButState = false


class CT1: UIViewController {
    
    @IBOutlet weak var DlyOnOffBut: UIButton!
    @IBOutlet weak var DlyTimeSlider: UISlider!
    @IBOutlet weak var DlyFdbckSlider: UISlider!
    @IBOutlet weak var DlyVolSlider: UISlider!
    
    @IBOutlet weak var VSTVolSlider: UISlider!
    
    @IBOutlet weak var H1But: UIButton!
    @IBOutlet weak var H2But: UIButton!
    @IBOutlet weak var H3But: UIButton!
    
    @IBOutlet weak var H1Stepper: UIStepper!
    @IBOutlet weak var H2Stepper: UIStepper!
    @IBOutlet weak var H3Stepper: UIStepper!
    
    @IBOutlet weak var H1Label: UILabel!
    @IBOutlet weak var H2Label: UILabel!
    @IBOutlet weak var H3Label: UILabel!
    
    @IBOutlet weak var H1VolSlider: UISlider!
    @IBOutlet weak var H2VolSlider: UISlider!
    @IBOutlet weak var H3VolSlider: UISlider!
    
    @IBOutlet weak var MasterVolSlider: UISlider!
    @IBOutlet weak var HarmonyVolSlider: UISlider!
    
    @IBOutlet weak var idStepper: UIStepper!
    @IBOutlet weak var idLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Force Light Mode
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }

        // Do any additional setup after loading the view.
        DlyVolSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        idLabel.text = "ID: \(DeviceID)"
        idStepper.value = Double(DeviceID)
        client = OSCClient(address: client.address, port: OUTport)
        
        H1VolSlider.value = 0
        H2VolSlider.value = 0
        H3VolSlider.value = 0
        
        VSTVolSlider.value = 0
        
        DlyTimeSlider.value = 0
        DlyFdbckSlider.value = 0.3
        
        MasterVolSlider.value = 0.8
        
        
    }
    
// ******* Delay Section
    
    @IBAction func DelayOnOffPressed(_ sender: Any) {
    let OSCAdress = "/ct/dly"
    DlyOnOffState = !DlyOnOffState
               
    if DlyOnOffState == true {
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
        DlyOnOffBut.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Blue"), for: .normal)
                   
        }else{
        
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
        client.send(message)
        DlyOnOffBut.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Blue"), for: .normal)
       
        }
    }
    @IBAction func DlyTimeChanged(_ sender: Any) {
        let OSCAdress = "/ct/dlytime"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), DlyTimeSlider.value)
        client.send(message)
    }
    @IBAction func DlyFdbckChanged(_ sender: Any) {
        let OSCAdress = "/ct/dlyfdbk"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), DlyFdbckSlider.value)
        client.send(message)
    }
    @IBAction func DlyVolChanged(_ sender: Any) {
        let OSCAdress = "/ct/dlyvol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), DlyVolSlider.value)
        client.send(message)
    }
    
// ******* Harmony Section
    
    @IBAction func H1ButPressed(_ sender: Any) {
        let OSCAdress = "/ct/h1tog"
        H1ButState = !H1ButState
                   
        if H1ButState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            H1But.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Blue"), for: .normal)
                       
            }else{
            
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            H1But.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Blue"), for: .normal)
           
            }
    }
    
    @IBAction func H2ButPressed(_ sender: Any) {
        let OSCAdress = "/ct/h2tog"
        H2ButState = !H2ButState
                   
        if H2ButState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            H2But.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Blue"), for: .normal)
                       
            }else{
            
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            H2But.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Blue"), for: .normal)
           
            }
    }
    
    @IBAction func H3ButPressed(_ sender: Any) {
        let OSCAdress = "/ct/h3tog"
        H3ButState = !H3ButState
                   
        if H3ButState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            H3But.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Blue"), for: .normal)
                       
            }else{
            
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            H3But.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Blue"), for: .normal)
           
            }
    }
    
    @IBAction func H1StepChange(_ sender: Any) {
        let OSCAdress = "/ct/h1transp"
        H1Label.text = "\(Int(H1Stepper.value))"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), H1Stepper.value)
        client.send(message)
    }
    
    @IBAction func H2StepChange(_ sender: Any) {
        let OSCAdress = "/ct/h2transp"
        H2Label.text = "\(Int(H2Stepper.value))"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), H2Stepper.value)
        client.send(message)
    }
    
    @IBAction func H3StepChange(_ sender: Any) {
        let OSCAdress = "/ct/h3transp"
        H3Label.text = "\(Int(H3Stepper.value))"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), H3Stepper.value)
        client.send(message)
    }
    
    @IBAction func H1VolChange(_ sender: Any) {
        let OSCAdress = "/ct/h1vol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), H1VolSlider.value)
        client.send(message)
    }
    
    @IBAction func H2VolChange(_ sender: Any) {
        let OSCAdress = "/ct/h2vol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), H2VolSlider.value)
        client.send(message)
    }
    
    @IBAction func H3VolChange(_ sender: Any) {
        let OSCAdress = "/ct/h3vol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), H3VolSlider.value)
        client.send(message)
    }
    
    @IBAction func HarmonyVolChange(_ sender: Any) {
        let OSCAdress = "/ct/hfader"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), HarmonyVolSlider.value)
        client.send(message)
    }
    
    //
    
    @IBAction func MasterVolChange(_ sender: Any) {
        let OSCAdress = "/ct/outvol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), MasterVolSlider.value)
        client.send(message)
    }
    @IBAction func VSTVolChange(_ sender: Any) {
        let OSCAdress = "/ct/vstvol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), VSTVolSlider.value)
        client.send(message)
    }
    @IBAction func idStepChange(_ sender: Any) {
        DeviceID = Int(idStepper.value)
        idLabel.text = "ID: \(DeviceID)"
        OUTport = 8000 + DeviceID
        client = OSCClient(address: client.address, port: OUTport)
        print ("Address is \(client.address)")
        print ("Port is \(client.port)")
    }
    
    
}
