//
//  MultiSampler.swift
//  SimpleOSC_3
//
//  Created by Gian on 14/09/2019.
//  Copyright © 2019 Giannino Clemente. Licensed under GPLv3. See LICENSE for more details.
//

import UIKit
import SwiftOSC

class VC2: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var idStepper: UIStepper!
    
    @IBOutlet weak var VolSlider: UISlider!
    @IBOutlet weak var SpeedSlider: UISlider!
    
    var LoopTog1State = false
    var LoopTog2State = false
    var LoopTog3State = false
    var LoopTog4State = false
    var LoopTog5State = false
    var LoopTog6State = false
    
    var RevTog1State = false
    var RevTog2State = false
    var RevTog3State = false
    var RevTog4State = false
    var RevTog5State = false
    var RevTog6State = false
    
    var SpeedTog1State = false
    var SpeedTog2State = false
    var SpeedTog3State = false
    var SpeedTog4State = false
    var SpeedTog5State = false
    var SpeedTog6State = false
    
    var PlayBut1State = false
    var PlayBut2State = false
    var PlayBut3State = false
    var PlayBut4State = false
    var PlayBut5State = false
    var PlayBut6State = false
    
    
    //PlayStops
    @IBOutlet weak var PlayBut1: UIButton!
    @IBOutlet weak var PlayBut2: UIButton!
    @IBOutlet weak var PlayBut3: UIButton!
    @IBOutlet weak var PlayBut4: UIButton!
    @IBOutlet weak var PlayBut5: UIButton!
    @IBOutlet weak var PlayBut6: UIButton!
    
    
    //Sliders
    @IBOutlet weak var Vol1: UISlider!
    @IBOutlet weak var Vol2: UISlider!
    @IBOutlet weak var Vol3: UISlider!
    @IBOutlet weak var Vol4: UISlider!
    @IBOutlet weak var Vol5: UISlider!
    @IBOutlet weak var Vol6: UISlider!
    
    // LoopTogs
    @IBOutlet weak var LoopTog1: UIButton!
    @IBOutlet weak var LoopTog2: UIButton!
    @IBOutlet weak var LoopTog3: UIButton!
    @IBOutlet weak var LoopTog4: UIButton!
    @IBOutlet weak var LoopTog5: UIButton!
    @IBOutlet weak var LoopTog6: UIButton!
    
    // RevTogs
    @IBOutlet weak var RevTog1: UIButton!
    @IBOutlet weak var RevTog2: UIButton!
    @IBOutlet weak var RevTog3: UIButton!
    @IBOutlet weak var RevTog4: UIButton!
    @IBOutlet weak var RevTog5: UIButton!
    @IBOutlet weak var RevTog6: UIButton!
    
    //SpeedTogs
    @IBOutlet weak var SpeedTog1: UIButton!
    @IBOutlet weak var SpeedTog2: UIButton!
    @IBOutlet weak var SpeedTog3: UIButton!
    @IBOutlet weak var SpeedTog4: UIButton!
    @IBOutlet weak var SpeedTog5: UIButton!
    @IBOutlet weak var SpeedTog6: UIButton!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Force Light Mode
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        idLabel.text = "ID: \(DeviceID)"
        idStepper.value = Double(DeviceID)
        //OUTport = 8000 + DeviceID
        //client = OSCClient(address: "255.255.255.255", port: OUTport)
        client = OSCClient(address: client.address, port: OUTport)
        
        //VolSliders Rotate
        VolSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        Vol1.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        Vol2.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        Vol3.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        Vol4.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        Vol5.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        Vol6.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        
    }
    
    @IBAction func idStepperChanged(_ sender: Any) {
        DeviceID = Int(idStepper.value)
        idLabel.text = "ID: \(DeviceID)"
        OUTport = 8000 + DeviceID
        client = OSCClient(address: client.address, port: OUTport)
    }
    
    @IBAction func SpeedSliderChanged(_ sender: Any) {
        let OSCAdress = "/ms/speed"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), SpeedSlider.value)
        client.send(message)
    }
    
    @IBAction func VolSliderChanged(_ sender: Any) {
        let OSCAdress = "/ms/vol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), VolSlider.value)
        client.send(message)
    }
    
    //PlayButs-------------------------------------------------------------------------*
    @IBAction func Play1Pressed(_ sender: Any) {
        let OSCAdress = "/ms/playstop1"
        PlayBut1State = !PlayBut1State
        
        if PlayBut1State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayBut1.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayBut1.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    @IBAction func Play2Pressed(_ sender: Any) {
        let OSCAdress = "/ms/playstop2"
        PlayBut2State = !PlayBut2State
        
        if PlayBut2State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayBut2.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayBut2.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    @IBAction func Play3Pressed(_ sender: Any) {
        let OSCAdress = "/ms/playstop3"
        PlayBut3State = !PlayBut3State
        
        if PlayBut3State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayBut3.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayBut3.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    @IBAction func Play4Pressed(_ sender: Any) {
        let OSCAdress = "/ms/playstop4"
        PlayBut4State = !PlayBut4State
        
        if PlayBut4State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayBut4.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayBut4.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    @IBAction func Play5Pressed(_ sender: Any) {
        let OSCAdress = "/ms/playstop5"
        PlayBut5State = !PlayBut5State
        
        if PlayBut5State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayBut5.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayBut5.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    @IBAction func Play6Pressed(_ sender: Any) {
        let OSCAdress = "/ms/playstop6"
        PlayBut6State = !PlayBut6State
        
        if PlayBut6State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayBut6.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayBut6.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    
    
    //LoopTogs-------------------------------------------------------------------------*
    
    @IBAction func LoopTog1Changed(_ sender: Any) {
        let OSCAdress = "/ms/1l"
        LoopTog1State = !LoopTog1State
        
        if LoopTog1State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog1.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog1.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func LoopTog2Changed(_ sender: Any) {
        let OSCAdress = "/ms/2l"
        LoopTog2State = !LoopTog2State
        
        if LoopTog2State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog2.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog2.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func LoopTog3Changed(_ sender: Any) {
        let OSCAdress = "/ms/3l"
        LoopTog3State = !LoopTog3State
        
        if LoopTog3State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog3.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog3.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func LoopTog4Changed(_ sender: Any) {
        let OSCAdress = "/ms/4l"
        LoopTog4State = !LoopTog4State
        
        if LoopTog4State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog4.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog4.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func LoopTog5Changed(_ sender: Any) {
        let OSCAdress = "/ms/5l"
        LoopTog5State = !LoopTog5State
        
        if LoopTog5State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog5.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog5.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func LoopTog6Changed(_ sender: Any) {
        let OSCAdress = "/ms/6l"
        LoopTog6State = !LoopTog6State
        
        if LoopTog6State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog6.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog6.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    
    //RevTogs
    @IBAction func RevTog1Changed(_ sender: Any) {
        let OSCAdress = "/ms/1r"
        RevTog1State = !RevTog1State
        
        if RevTog1State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog1.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog1.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func RevTog2Changed(_ sender: Any) {
        let OSCAdress = "/ms/2r"
        RevTog2State = !RevTog2State
        
        if RevTog2State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog2.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog2.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func RevTog3Changed(_ sender: Any) {
        let OSCAdress = "/ms/3r"
        RevTog3State = !RevTog3State
        
        if RevTog3State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog3.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog3.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func RevTog4Changed(_ sender: Any) {
        let OSCAdress = "/ms/4r"
        RevTog4State = !RevTog4State
        
        if RevTog4State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog4.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog4.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func RevTog5Changed(_ sender: Any) {
        let OSCAdress = "/ms/5r"
        RevTog5State = !RevTog5State
        
        if RevTog5State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog5.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog5.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func RevTog6Changed(_ sender: Any) {
        let OSCAdress = "/ms/6r"
        RevTog6State = !RevTog6State
        
        if RevTog6State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog6.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog6.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    
    
    //SpeedTogs-------------------------------------------------------------*
    @IBAction func SpeedTog1Changed(_ sender: Any) {
        let OSCAdress = "/ms/1s"
        SpeedTog1State = !SpeedTog1State
        
        if SpeedTog1State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            SpeedTog1.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            SpeedTog1.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func SpeedTog2Changed(_ sender: Any) {
        let OSCAdress = "/ms/2s"
        SpeedTog2State = !SpeedTog2State
        
        if SpeedTog2State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            SpeedTog2.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            SpeedTog2.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func SpeedTog3Changed(_ sender: Any) {
        let OSCAdress = "/ms/3s"
        SpeedTog3State = !SpeedTog3State
        
        if SpeedTog3State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            SpeedTog3.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            SpeedTog3.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func SpeedTog4Changed(_ sender: Any) {
        let OSCAdress = "/ms/4s"
        SpeedTog4State = !SpeedTog4State
        
        if SpeedTog4State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            SpeedTog4.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            SpeedTog4.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func SpeedTog5Changed(_ sender: Any) {
        let OSCAdress = "/ms/5s"
        SpeedTog5State = !SpeedTog5State
        
        if SpeedTog5State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            SpeedTog5.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            SpeedTog5.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    @IBAction func SpeedTog6Changed(_ sender: Any) {
        let OSCAdress = "/ms/6s"
        SpeedTog6State = !SpeedTog6State
        
        if SpeedTog6State == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            SpeedTog6.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Green"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            SpeedTog6.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Green"), for: .normal)
        }
    }
    
    //VolSliders---------------------------------------------------------*
    @IBAction func Slider1Changed(_ sender: Any) {
        let OSCAdress = "/ms/vol1"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), Vol1.value)
        client.send(message)
    }
    @IBAction func Slider2Changed(_ sender: Any) {
        let OSCAdress = "/ms/vol2"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), Vol2.value)
        client.send(message)
    }
    @IBAction func Slider3Changed(_ sender: Any) {
        let OSCAdress = "/ms/vol3"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), Vol3.value)
        client.send(message)
    }
    @IBAction func Slider4Changed(_ sender: Any) {
        let OSCAdress = "/ms/vol4"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), Vol4.value)
        client.send(message)
    }
    @IBAction func Slider5Changed(_ sender: Any) {
        let OSCAdress = "/ms/vol5"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), Vol5.value)
        client.send(message)
    }
    @IBAction func Slider6Changed(_ sender: Any) {
        let OSCAdress = "/ms/vol6"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), Vol6.value)
        client.send(message)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
