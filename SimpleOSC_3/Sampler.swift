//
//  ViewController.swift
//  SimpleOSC_3
//
//  Created by Gian on 14/09/2019.
//  Copyright © 2019 Giannino Clemente. Licensed under GPLv3. See LICENSE for more details.
//

import UIKit
import SwiftOSC

//var OUTport = 7999
//var client = OSCClient(address: "255.255.255.255", port: OUTport)

var PlayButtonState = false
var LoopTogState = false
var PitchTogState = false
var SpeedButState = false
var RevTogState = false


/*
var message = OSCMessage(
    OSCAddressPattern("/"),
    100,
    5.0,
    "Hello World",
    Blob(),
    true,
    false,
    nil,
    impulse,
    Timetag(1)
*/


class ViewController: UIViewController {

    //  Define Elements
    @IBOutlet weak var PlayStopButton: UIButton!
    @IBOutlet weak var LoopTog: UIButton!
    @IBOutlet weak var PitchTog: UIButton!
    @IBOutlet weak var SpeedBut: UIButton!
    @IBOutlet weak var RevTog: UIButton!
    @IBOutlet weak var SpeedSlider: UISlider!
    @IBOutlet weak var PosSlider: UISlider!
    @IBOutlet weak var VolSlider: UISlider!
    @IBOutlet weak var PauseButton: UIButton!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var idStepper: UIStepper!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Force Light Mode
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        // Make PosSlider Tappable
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PosSliderTapped(gestureRecognizer:)))
            self.PosSlider.addGestureRecognizer(tapGestureRecognizer)
        
        
        VolSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        idLabel.text = "ID: \(DeviceID)"
        idStepper.value = Double(DeviceID)
        client = OSCClient(address: client.address, port: OUTport)
    }
    
    //PlayStop PRESSED
    
    @IBAction func PlayStopButtonPressed(_ sender: Any) {
        let OSCAdress = "/1/playtoggle"
        PlayButtonState = !PlayButtonState
        
        if PlayButtonState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PlayStopButton.setBackgroundImage(#imageLiteral(resourceName: "StopButton"), for: .normal)
            
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PlayStopButton.setBackgroundImage(#imageLiteral(resourceName: "PlayButton"), for: .normal)
        }
        
    }
    
    // BUTTONS ---------
    
    @IBAction func LoopTogPressed(_ sender: Any) {

    let OSCAdress = "/1/looptoggle"
    LoopTogState = !LoopTogState
        
        if LoopTogState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            LoopTog.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Orange"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            LoopTog.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Orange"), for: .normal)
        }
        
    }
  
    
    @IBAction func PitchTogPressed(_ sender: Any) {
   
   
        let OSCAdress = "/1/pitchtoggle"
        PitchTogState = !PitchTogState
        
        if PitchTogState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            PitchTog.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Orange"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            PitchTog.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Orange"), for: .normal)
        }
        
    }
    
    
    @IBAction func ResetSpeedButPressed(_ sender: Any) {
        SpeedSlider.value = 0.5
        let OSCAdress = "/1/resetspeed"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
        
    }
    
    @IBAction func ReverseButPressed(_ sender: Any) {
        let OSCAdress = "/1/reversetoggle"
        RevTogState = !RevTogState
        
        if RevTogState == true {
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
            client.send(message)
            RevTog.setBackgroundImage(#imageLiteral(resourceName: "ToggleON_Orange"), for: .normal)
            
        }else{
            let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
            client.send(message)
            RevTog.setBackgroundImage(#imageLiteral(resourceName: "ToggleOFF_Orange"), for: .normal)
        }
        
    }
    
    @IBAction func PauseButtonPressed(_ sender: Any) {
        let OSCAdress = "/1/pausebutton"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    
    //Change ID Stepper
    
    @IBAction func idStepperChanged(_ sender: Any) {
        DeviceID = Int(idStepper.value)
        idLabel.text = "ID: \(DeviceID)"
        OUTport = 8000 + DeviceID
        client = OSCClient(address: client.address, port: OUTport)
        print ("Address is \(client.address)")
        print ("Port is \(client.port)")
    }
    
    
    // SLIDERS ---------
    
    @IBAction func VolSliderChanged(_ sender: Any) {
        let OSCAdress = "/1/vol"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), VolSlider.value)
        client.send(message)
    }
    
    @IBAction func SpeedSliderChanged(_ sender: Any) {
        let OSCAdress = "/1/speed"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), SpeedSlider.value)
        client.send(message)
        SpeedSlider.value = SpeedSlider.value
    }
    
    
    
    @IBAction func PosSliderChanged(_ sender: Any) {
        let OSCAdress = "/1/playback"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), PosSlider.value)
        client.send(message)
    }
    
    @objc func PosSliderTapped(gestureRecognizer: UIGestureRecognizer) {

        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)

        let positionOfSlider: CGPoint = PosSlider.frame.origin
        let widthOfSlider: CGFloat = PosSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(PosSlider.maximumValue) / widthOfSlider)

        PosSlider.setValue(Float(newValue), animated: false)
        
        let OSCAdress = "/1/playback"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), PosSlider.value)
        client.send(message)
    }
    
    
}

